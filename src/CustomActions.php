<?php
/**
 * Custom actions plugin for Craft CMS 3.x
 *
 * Plugin for custom site actions
 *
 * @link      https://pongping.studio
 * @copyright Copyright (c) 2019 PongPing
 */

namespace pongping\customactions;


use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use GuzzleHttp;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    PongPing
 * @package   CustomActions
 * @since     1.0.0
 *
 */
class CustomActions extends Plugin
{
	// Static Properties
	// =========================================================================

	/**
	 * Static property that is an instance of this plugin class so that it can be accessed via
	 * CustomActions::$plugin
	 *
	 * @var CustomActions
	 */
	public static $plugin;

	// Public Properties
	// =========================================================================

	/**
	 * To execute your plugin’s migrations, you’ll need to increase its schema version.
	 *
	 * @var string
	 */
	public $schemaVersion = '1.0.0';

	// Public Methods
	// =========================================================================

	/**
	 * Set our $plugin static property to this class so that it can be accessed via
	 * CustomActions::$plugin
	 *
	 * Called after the plugin class is instantiated; do any one-time initialization
	 * here such as hooks and events.
	 *
	 * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
	 * you do not need to load it in your init() method.
	 *
	 */
	public function init()
	{
		parent::init();
		self::$plugin = $this;

		// Register our site routes

		Event::on(
			UrlManager::class,
			UrlManager::EVENT_REGISTER_SITE_URL_RULES,
			function (RegisterUrlRulesEvent $event) {
				//$event->rules['verify'] = 'customactions/verify';
			}
		);

		// Do something after we're installed
		Event::on(
			Plugins::class,
			Plugins::EVENT_AFTER_INSTALL_PLUGIN,
			function (PluginEvent $event) {
				if ($event->plugin === $this) {
					// We were just installed
				}
			}
		);


		Event::on(\Solspace\Freeform\Services\FormsService::class, \Solspace\Freeform\Services\FormsService::EVENT_FORM_VALIDATE, function(Event $event) {

			Craft::warning('In event handler', 'customactions');



			// Get the reCAPTCHA response code to validate.
			$captcha = Craft::$app->getRequest()->getParam('token');


			if (strlen($captcha) > 0){
				$base = "https://www.google.com/recaptcha/api/siteverify";

				//$settings = CraftRecaptcha::$plugin->getSettings();
				$params = array(
					//'secret' =>  '6Ld_8JAUAAAAANY4FN19lZ6KVQI3vnb5X2gC4xuP',
					'secret' =>  '6LctqJcUAAAAAAgbUGEIPeaGSWCzj2VfEFIVBZ-l',
					'response' => $captcha
				);

				$client = new GuzzleHttp\Client();
				$response = $client->request('POST', $base, ['form_params' => $params]);

				if($response->getStatusCode() == 200)
				{
					$json = json_decode($response->getBody());

					if($json->success && $json->score > 0.5) {} else {
						$event->addErrorToForm('Dit is een verplicht veld.');
					}
				} else {
					$event->addErrorToForm('Dit is een verplicht veld.');
				}
			} else{
				$event->addErrorToForm('Dit is een verplicht veld.');
			}
		});

		/**
		 * Logging in Craft involves using one of the following methods:
		 *
		 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
		 * Craft::info(): record a message that conveys some useful information.
		 * Craft::warning(): record a warning message that indicates something unexpected has happened.
		 * Craft::error(): record a fatal error that should be investigated as soon as possible.
		 *
		 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
		 *
		 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
		 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
		 *
		 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
		 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
		 *
		 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
		 */
		Craft::info(
			Craft::t(
				'customactions',
				'{name} plugin loaded',
				['name' => $this->name]
			),
			__METHOD__
		);
	}

	// Protected Methods
	// =========================================================================

}
