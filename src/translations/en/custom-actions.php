<?php
/**
 * Custom actions plugin for Craft CMS 3.x
 *
 * Plugin for custom site actions
 *
 * @link      https://pongping.studio
 * @copyright Copyright (c) 2019 PongPing
 */

/**
 * Custom actions en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('customactions', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    PongPing
 * @package   CustomActions
 * @since     1.0.0
 */
return [
    'Custom actions plugin loaded' => 'Custom actions plugin loaded',
];
