# Custom actions plugin for Craft CMS 3.x

Plugin for custom site actions

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /customactions

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Custom actions.

## Custom actions Overview

-Insert text here-

## Configuring Custom actions

-Insert text here-

## Using Custom actions

-Insert text here-

## Custom actions Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [PongPing](https://pongping.studio)
